var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET home page. */
router.get('/', function (req, res) {
    models.Statistic.findAll().then(function (statistics) {
        res.render('index', {title: "Timeline of the \"Greek Bailout Fund\" crowd funding campaign @ Indiegogo", data: statistics});
    })

});

module.exports = router;