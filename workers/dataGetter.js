var https = require('https');
var cheerio = require('cheerio');
var models = require('../models');

var options = {
    host: 'www.indiegogo.com',
    port: 443,
    path: '/projects/greek-bailout-fund/'
};

function fetch() {
    https.get(options, function (response) {
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            var $ = cheerio.load(str);
            var pledged = $("div.i-balance > span > span").contents().text().replace(/\,/g, "").replace(".","").replace("\u20ac", "");
            var pledgers = $("span.i-raised-funders").contents().text().replace(" people", "").replace(/\,/g, "");
            newStatistic(pledged, pledgers);
            return 0;
        });
    }).end();
};

setInterval(fetch, 600000);

function newStatistic(pledged, pledgers) {
    models.Statistic.create({
        amountOfMoney: pledged,
        amountOfPeople: pledgers
    });
}