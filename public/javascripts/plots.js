"use strict"

$(function () {
    var ctx = document.getElementById("myLineChart").getContext("2d");
    var labelsData = [];
    var moneyData = [];
    var peopleData = [];
    for (var i = 0; i < statistics_data.length; i++) {
        //if (i%10 == 0)
        labelsData.push(statistics_data[i].createdAt.substring(6, 16).replace("-","/").replace("T"," "));
        moneyData.push(statistics_data[i].amountOfMoney);
        peopleData.push(statistics_data[i].amountOfPeople);
    }
    var data = {
        labels: labelsData,
        datasets: [
            {
                label: "Euros pledged",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: moneyData
            },
            {
                label: "Number of people",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: peopleData
            }
        ]
    };
    var options = {

        ///Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,

        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",

        //Number - Width of the grid lines
        scaleGridLineWidth: 1,

        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,

        //Boolean - Whether the line is curved between points
        bezierCurve: true,

        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.4,

        //Boolean - Whether to show a dot for each point
        pointDot: false,

        //Number - Radius of each point dot in pixels
        pointDotRadius: 1,

        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,

        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,

        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,

        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,

        //Boolean - Whether to fill the dataset with a colour
        datasetFill: true,

        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\" style=\"list-style: none;\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"

    };
    var myLineChart = new Chart(ctx).Line(data, options);

    document.getElementById('js-legend').innerHTML = myLineChart.generateLegend();

});
