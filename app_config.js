/**
 *  Application configuration
 */
exports.config = {
    name: 'BailoutGreece',
    port: process.env.PORT,
    staticFilesDirectory: '/public',
    LessInputDirectory: '/less_styles',
    LessOutputDirectory: '/public',
    viewsDirectory: '/views',
    templateEngine: 'jade'
};