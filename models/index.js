"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize"); // Initialze Module
var seqInstance = new Sequelize('bailoutgreece', 'postgres', 'postgres', { // Create instance of Sequelize
    host: 'localhost',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

// Keeps track of the application models, Sequelize instance and module.
var db = {};

// Parse models folder files and add them to the db dictionary.
// Adds models
fs
    .readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function (file) {
        var model = seqInstance["import"](path.join(__dirname, file));
        db[model.name] = model;
    });

// Adds model associations (belongsTo, hasOne etc)
Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

// Assign values to Sequelize module and instance for the db dictionary.
db.seqInstance = seqInstance;
db.Sequelize = Sequelize;

module.exports = db;