"use strict";

function Define_Statistic(sequelize, DataTypes) {
    var Statistic = sequelize.define('Statistic',
        //Model Members
        {
            amountOfMoney: {
                type: DataTypes.INTEGER,
                field: 'pledged'
            },
            amountOfPeople: {
                type: DataTypes.INTEGER,
                field: 'pledgers'
            }
        });

    return Statistic;
}

module.exports = Define_Statistic;